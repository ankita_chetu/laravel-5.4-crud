<?php namespace App\Libraries;

use Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Fmessages
{
    static $BidEmailSend = "Email has been successfully send out and Your bid was the highest from all the bids.. Congrats!";
    
    static $productcreate = "Product Successfully Created";
    static $producterror = "There is an error processing the product";
    static $productdelete = "Product Successfully Deleted";
    static $productupdate = "Product Updated Successfully";
    static $productnochange = "No changes Made in Current Product";

    static $Bidcreated = "New Bid has been successfully Placed";
    static $Bidnochange = "There is an error processing the Bid";

    static function flashmessage($name, $class)
    {
        Session::flash('message', self::$$name);
        Session::flash('alert-class', $class);
        return true;
    }
}


