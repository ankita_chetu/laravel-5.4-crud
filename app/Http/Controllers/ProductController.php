<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\User;
use App\Product;
use App\Libraries\Fmessages;
use App\Permission;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use Mailgun\Mailgun;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $product = DB::table('products')->whereNull('deleted_at')->orderBy('created_at', 'DESC')->paginate(10);
        return view('product.template', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
       return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            //'email' => 'required|email|max:255|unique:users',
            //'password' => 'required|confirmed|min:6',
            //'password_confirmation' => 'required',
            'sku' => 'required|alpha_num|max:255',
            'price' => 'required|integer',
            'description' => 'required|max:255',
        ]);

        $inserTeam =  Product::create([
            'name' => $request['name'],
            'sku' => $request['sku'],
            'price' => $request['price'],
            'description' => $request['description'],
        ]);


        if ($inserTeam) {
            Fmessages::flashmessage('productcreate', 'alert-success');
        } else {
            Fmessages::flashmessage('producterror', 'alert-danger');
        }
        return redirect('product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $ipAddress = Product::IpAddress();
        $checkIP = DB::table('ip_address')->where([['product_id','=',$id],['ip','=',$ipAddress]])->first();
        $getProductIp = DB::table('ip_address')->where('product_id','=',$id)->orderBy('created_at', 'DESC')->first(); $getLatestBid = DB::table('bids')->where('product_id','=',$id)->orderBy('created_at', 'DESC')->first();        
        if(! $checkIP) {
            $InsertIP = DB::table('ip_address')->insert(['ip' => $ipAddress, 'product_id' => $id]);
        }

        $product = DB::table('products')->where('id', '=', $id)->first();
        return view('product.show', compact('product','getProductIp','getLatestBid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $product = DB::table('products')->where('id', '=', $id)->first();
        return view('product.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'sku' => 'required|alpha_num|max:255',
            'price' => 'required|integer',
            //'email' => 'required|email|unique:users,email,' . $id . '',
        ]);

        $product = $request->all();

        $productupdate = DB::table('products')->where('id', $id)->update(['name' => $product['name'], 'sku' => $product['sku'], 'price' => $product['price'], 'description' => $product['description'] ]);

        if ($productupdate) {
            Fmessages::flashmessage('productupdate', 'alert-success');
        } else {
            Fmessages::flashmessage('productnochange', 'alert-warning');
        }
        return redirect('product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $productd = Product::find($id);
        $productDel = DB::table('products')->where('id', '=', $id)->update(['deleted_at' => Carbon::now()]);
        if ($productDel) {
            Fmessages::flashmessage('productdelete', 'alert-success');
        } else {
            Fmessages::flashmessage('clienterror', 'alert-danger');
        }
        return redirect('product');
    }

    /**
     * Bid the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function bid($id, Request $request)
    {
        $this->validate($request, [
            'bidprice' => 'required|integer',
            'bidemail' => 'required|email',
        ]);

        $product = $request->all();
        
        $lastMaxBid = DB::table('bids')->where('product_id',$id)->max('bid_price');

        $productupdate = DB::table('bids')->insert(['product_id' => $id, 'bid_price' => $product['bidprice'], 'bid_email' => $product['bidemail'],'created_at'=>carbon::now() ]);

        # Instantiate the client.
        $mgClient = new Mailgun('key-2b608112b0d46ca04bbf339627bf225c');
        $domain = "sandbox300d607b5ff84d3583fbfe3622b835aa.mailgun.org";

        if($lastMaxBid < $product['bidprice']) {
            
            /* send out email to admin */

            # Make the call to the client.
        
        /*    $result = $mgClient->sendMessage("$domain",
        array('from'    => 'Mailgun Sandbox <postmaster@sandbox300d607b5ff84d3583fbfe3622b835aa.mailgun.org>',
              'to'      => 'Ankit Aggarwal <ankitprofessional07@gmail.com>',
              'subject' => 'Hello Ankit Aggarwal',
              'text'    => 'A new user with email id '.$product['bidemail'].' have the highest bidding of price '.$product['bidprice']));
        */

        Fmessages::flashmessage('BidEmailSend', 'alert-success');
        return redirect('product');
        
        }

        if ($productupdate) {
            Fmessages::flashmessage('Bidcreated', 'alert-success');
        } else {
            Fmessages::flashmessage('Bidnochange', 'alert-warning');
        }
        return redirect('product');
    }


}