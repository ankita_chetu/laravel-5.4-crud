<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Ankit',
            'email' => 'ankitprofessional07@gmail.com',
            'password' => bcrypt('Chetu!@#'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);

        for($i=1; $i<=100; $i++){
            DB::table('products')->insert([
                'name' => 'product'.$i,
                'sku' => 'sku000'.$i,
                'price' => '101'.$i,
                'description' => 'product descrition for sku id sku000'.$i,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
            ]);
        }
    }
}
