@extends('app')

@section('content')
<div class="right_col" role="main">
       <div class="col-md-2 pull-right">
    </div>

    <h1>Team(s)</h1>
    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <div class="table-responsive">
        {!! Form::open(['id'=>'access','method' => 'POST', 'url'=>['permission']]) !!}
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr class="bg-info">
                <th>No. </th>
                <th>Team Name</th>
                <th>Email</th>
                <th>Create Team(s)</th>
                <th>Access All Clients</th>
                <th>Admin Rights</th>
            </tr>
        </thead>

        <tbody>

            <?php $i = 1;  ?>
@if(count($agency) == 0)
<tr><td style="text-align:center" colspan="6"><b>No Team for this Group</b></td></tr>
    @else
            @foreach ($agency as $client)

            <tr>
                <td><b>{{ $i }}</b></td>
                <td><b>{{ $client->name }}</b></td>
                <td><b>{{ $client->email }}</b></td>
                <?php
                if(!empty($permit->team_permit)){
                $checked_Team = (in_array($client->id,$permit->team_permit)=='1')?'true':'';
                    }
                    else {
                        $checked_Team ='';
                    }
                    if(!empty($permit->access_client)){
                $checked_accessClient = (in_array($client->id,$permit->access_client)=='1')?'true':'';
                }
                    else {
                        $checked_accessClient='';
                    }

                    //echo'<pre>';print_r($adminUser); echo'</pre>';die();

                if(!empty($adminUser)){
                    $checked_accessAdmin = (in_array($client->id,$adminUser)=='1')?'true':'';
                    $disabled = (in_array($client->id,$adminUser)=='1')?'disabled':'';
                    $checked_accessClient = (in_array($client->id,$adminUser)=='1')?'true':$checked_accessClient;
                    $checked_Team =(in_array($client->id,$adminUser)=='1')?'true':$checked_Team;
                }
                else {
                    $checked_accessAdmin='';
                    $disabled = '';
                    $checked_Team = (in_array($client->id,$permit->team_permit)=='1')?'true':$checked_Team;
                    $checked_accessClient = (in_array($client->id,$permit->access_client)=='1')?'true':$checked_accessClient;

                }
                    if(Auth::user()->id  == $client->id){
                        $currentAdminDisable='hidden';
                        $currentAdmin='&#10004';
                    }
                    else {
                        $currentAdminDisable='';
                        $currentAdmin='';
                    }
                ?>
                <td>{!! Form::checkbox('team_permit[]', $client->id, $checked_Team, array($disabled)) !!}</td>
                <td>{!! Form::checkbox('access_client[]', $client->id, $checked_accessClient,  array($disabled)) !!}</td>
                <td>{!! Form::checkbox('access_admin[]', $client->id, $checked_accessAdmin, array($currentAdminDisable)) !!} {!! $currentAdmin !!} </td>
            </tr>
            <?php $i++; ?>
            @endforeach
@endif


        </tbody>

    </table>

        </div>
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}



</div>


{{--<td><b>{{ date("F j, Y, g:i a", strtotime($client->created_at)) }}</b></td>--}}
{{--<td style="text-align:center">--}}
{{--{!! Form::open(['id'=>'access'.$client->id,'method' => 'POST', 'url'=>['clients/access']]) !!}--}}
{{--{!! Form::hidden('clientID',$client->id,['class'=>'form-control','readonly']) !!}--}}
{{--{!! Form::submit('Grant Access', ['class' => 'btn btn-primary', 'id' => 'grant']) !!}--}}
{{--{!! Form::close() !!}--}}
{{--</td>--}}
{{----}}

{{--<td><a href="{{route('agency.edit',$client->id)}}" class="btn btn-warning">Update / Read</a></td>--}}
{{--<td>--}}
{{--{!! Form::open(['method' => 'DELETE','id' => 'deleteClient'.$client->id, 'route'=>['agency.destroy', $client->id]]) !!}--}}
{{--{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}--}}
{{--{!! Form::close() !!}--}}
{{--</td>--}}



{{--<script src="{{ URL::asset('js/bootbox.min.js') }}"></script>--}}
{{--<script>--}}
{{--$(document).ready(function () {--}}
    {{--$('form').submit(function (e) {--}}
        {{--var abc = $(this).attr("id");--}}
        {{--var but = "#" + abc;--}}
        {{--var match = abc.replace(/\d+/g, '');--}}

        {{--if (match == 'deleteClient') {--}}
            {{--e.preventDefault()--}}
            {{--bootbox.confirm("Are you sure to Delete Client?", function (result) {--}}
                {{--if (result) {--}}
                    {{--$(but).unbind();--}}
                    {{--$(but).submit();--}}
                {{--}--}}
            {{--});--}}
            {{--// return confirm('Are you sure you want to delete?');--}}
        {{--}--}}

        {{--if (match == 'access') {--}}
            {{--e.preventDefault()--}}
            {{--bootbox.confirm("Grant Access to Client?", function (result) {--}}

                {{--if (result) {--}}
                    {{--$(but).unbind();--}}
                    {{--$(but).submit();--}}
                {{--}--}}
            {{--});--}}
            {{--//   return bootbox.confirm('Grant Access to client?');--}}
        {{--}--}}

    {{--});--}}

{{--});--}}

{{--$(document).on("change","#clientSearch", function(){--}}
    {{--var sort = $(this).val();--}}
    {{--if(sort == ''){--}}
        {{--var hitUrl ='{{URL('clients')}}';--}}
    {{--}--}}
    {{--if(sort == 'grant'){--}}
        {{--var hitUrl ='{{URL('clients?sort=grant')}}';--}}
    {{--}--}}
    {{--if(sort == 'granted'){--}}
        {{--var hitUrl ='{{URL('clients?sort=granted')}}';--}}
    {{--}--}}
    {{--window.location = hitUrl ;--}}
{{--});--}}
{{--</script>--}}
@endsection