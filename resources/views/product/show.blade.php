@extends('layouts.app')

@section('content')

<div class="container" role="main">
    <a href="{{ url('/product') }}" class="btn btn-success pull-right">Back</a>
    <h1>Bid Product</h1>

@if($getProductIp)
<div class="alert alert-success">
        <ul>
            <li><b>Last Product viewed by IP : {{ $getProductIp->ip }}</b></li>
        </ul>
    </div>
@endif
@if($getLatestBid)
<div class="alert alert-success">
        <ul>
            <li><b>Last Bid Placed is of Price : {{ $getLatestBid->bid_price }} and email id of bidder : {{ $getLatestBid->bid_email }} </b></li>
        </ul>
    </div>
@endif

    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="client-form">
    {!! Form::model($product,['method' => 'PATCH','id'=>'NewClientId','data-toggle'=>"validator",'route'=>['product.bid',$product->id]],['data-toggle'=>"validator"]) !!}

    <div class="form-group">
        <div class="col-md-6 col-xs-12">
            {!! Form::label('productname', 'Product Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Product Name','readonly']) !!}
        </div>
        <div class="col-md-6 col-xs-12">
            {!! Form::label('bidamount', 'Bid Price:') !!}
            {!! Form::text('bidprice',null,['class'=>'form-control','placeholder'=>'Bid Price']) !!}
        </div>
        <div class="clearfix"></div>
    </div>
        <div class="form-group">

            <div class="col-md-6 col-xs-12">
                {!! Form::label('sku', 'SKU:') !!}
                {!! Form::text('sku',null,['class'=>'form-control', 'readonly']) !!}
            </div>
            <div class="col-md-6 col-xs-12">
            {!! Form::label('bidemail', 'Bidder Email:') !!}
            {!! Form::email('bidemail',null,['class'=>'form-control','placeholder'=>'Bidder Email']) !!}
        </div>
            <div class="clearfix"></div>
        </div>


        <div class="form-group">

            <div class="col-md-6 col-xs-12">
                {!! Form::label('price', 'Price:') !!}
                {!! Form::text('price',null,['class'=>'form-control','readonly']) !!}
            </div>
            <div class="col-md-6 col-xs-12" style="text-align:right">   {!! Form::submit('Place a Bid', ['class' => 'btn btn-primary']) !!} </div>
            
            <div class="clearfix"></div>
        </div>


        <div class="form-group">

            <div class="col-md-6 col-xs-12">
                {!! Form::label('description', 'Description:') !!}
                {!! Form::textarea('description',null,['class'=>'form-control','readonly']) !!}
            </div>
            <div class="clearfix"></div>
        </div>

      <div class="form-group">
          <!-- <div class="col-md-12">   {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!} </div> -->
          <div class="clearfix"></div>
    </div>
    {!! Form::close() !!}
        </div>
</div>



@endsection
