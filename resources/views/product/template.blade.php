@extends('layouts.app')

@section('content')
<div class="container" role="main">
      
<a href="{{ url('/product/create') }}" class="btn btn-success pull-right">Create Product</a>
    <h1>Product(s)</h1>

    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr class="bg-info">
                <th>No.</th>
                <th>Product Name</th>
                <th>SKU</th>
                <th>Price</th>
                <th>Created at</th>
                <th colspan="4">Actions</th>
            </tr>
        </thead>

        <tbody>
            <?php $i = 1;  ?>
@if(count($product) == 0)
<tr><td style="text-align:center" colspan="6"><b>No New Products found.</b></td></tr>
    @else
            @foreach ($product as $client)
            <tr>
                <td><b>{{ $i }}</b></td>
                <td><b>{{ $client->name }}</b></td>
                <td><b>{{ $client->sku }}</b></td>
                <td><b>{{ $client->price }}</b></td>
                <td><b>{{ date("F j, Y, g:i a", strtotime($client->created_at)) }}</b></td>
                {{--<td style="text-align:center">--}}
                    {{--{!! Form::open(['id'=>'access'.$client->id,'method' => 'POST', 'url'=>['clients/access']]) !!}--}}
                    {{--{!! Form::hidden('clientID',$client->id,['class'=>'form-control','readonly']) !!}--}}
                    {{--{!! Form::submit('Grant Access', ['class' => 'btn btn-primary', 'id' => 'grant']) !!}--}}
                    {{--{!! Form::close() !!}--}}
                {{--</td>--}}
              {{----}}

                <td><a href="{{route('product.edit',$client->id)}}" class="btn btn-warning">Update</a>
                <a href="{{route('product.show',$client->id)}}" class="btn btn-warning">View</a></td>
                <td>
                    {!! Form::open(['method' => 'DELETE','id' => 'deleteClient'.$client->id, 'route'=>['product.destroy', $client->id]]) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}


                </td>
              

            </tr>
            <?php $i++; ?>
            @endforeach
@endif

        </tbody>

    </table>
        </div>
    {!! str_replace('/?','?',$product->render()) !!}


</div>
<!-- <script src="{{ URL::asset('js/bootbox.min.js') }}"></script>
<script>
$(document).ready(function () {
    $('form').submit(function (e) {
        var abc = $(this).attr("id");
        var but = "#" + abc;
        var match = abc.replace(/\d+/g, '');

        if (match == 'deleteClient') {
            e.preventDefault()
            bootbox.confirm("Are you sure to Delete Client?", function (result) {
                if (result) {
                    $(but).unbind();
                    $(but).submit();
                }
            });
            // return confirm('Are you sure you want to delete?');
        }

        if (match == 'access') {
            e.preventDefault()
            bootbox.confirm("Grant Access to Client?", function (result) {

                if (result) {
                    $(but).unbind();
                    $(but).submit();
                }
            });
            //   return bootbox.confirm('Grant Access to client?');
        }

    });

});

</script> -->
@endsection