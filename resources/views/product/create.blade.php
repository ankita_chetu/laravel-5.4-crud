@extends('layouts.app')

@section('content')

    <div class="container" role="main">
        <a href="{{ url('/product') }}" class="btn btn-success pull-right">Back</a>
        <h1>Create New Product</h1>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="clearfix"></div>

        <div class="client-form">
            <form role="form" method="POST" action="{{ url('/product') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    {!! Form::label('name', 'Name') !!}<span class="red-star">*</span>
                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}">

                </div>
            </div>
                <div class="clearfix"></div>
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    {!! Form::label('sku', 'SKU') !!}<span class="red-star">*</span>
                    <input type="text" class="form-control" placeholder="SKU" name="sku" value="{{ old('sku') }}">
                </div>
            </div>
            <div class="clearfix"></div>

             <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    {!! Form::label('price', 'Price') !!}<span class="red-star">*</span>
                    <input type="text" class="form-control" placeholder="Price" name="price" value="{{ old('price') }}">
                </div>
            </div>
            <div class="clearfix"></div>

             <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    {!! Form::label('description', 'Description') !!}<span class="red-star">*</span>
                    <input type="text" class="form-control" placeholder="Description" name="description" value="{{ old('description') }}">
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="clearfix"></div>

            <div class="form-group">
                <div class="col-md-3">
                    {!! Form::submit('Save', ['class' => 'btn btn-primary btn-block']) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
