@extends('layouts.app')

@section('content')

<div class="container" role="main">
    <a href="{{ url('/product') }}" class="btn btn-success pull-right">Back</a>
    <h1>Update Product</h1>


    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="client-form">
    {!! Form::model($product,['method' => 'PATCH','id'=>'NewClientId','data-toggle'=>"validator",'route'=>['product.update',$product->id]],['data-toggle'=>"validator"]) !!}

    <div class="form-group">
        <div class="col-md-6 col-xs-12">
            {!! Form::label('productname', 'Product Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Team Name']) !!}
        </div>

        <div class="clearfix"></div>
    </div>
        <div class="form-group">

            <div class="col-md-6 col-xs-12">
                {!! Form::label('sku', 'SKU:') !!}
                {!! Form::text('sku',null,['class'=>'form-control']) !!}
            </div>
            <div class="clearfix"></div>
        </div>


        <div class="form-group">

            <div class="col-md-6 col-xs-12">
                {!! Form::label('price', 'Price:') !!}
                {!! Form::text('price',null,['class'=>'form-control']) !!}
            </div>
            <div class="clearfix"></div>
        </div>


        <div class="form-group">

            <div class="col-md-6 col-xs-12">
                {!! Form::label('description', 'Description:') !!}
                {!! Form::textarea('description',null,['class'=>'form-control']) !!}
            </div>
            <div class="clearfix"></div>
        </div>

      <div class="form-group">
          <div class="col-md-12">   {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!} </div>
          <div class="clearfix"></div>
    </div>
    {!! Form::close() !!}
        </div>
</div>



@endsection
